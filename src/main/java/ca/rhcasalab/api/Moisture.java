package ca.rhcasalab.api;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.MongoEntity;

@MongoEntity(collection="moisture")
public class Moisture extends PanacheMongoEntity {
  public String sensor;
  public Integer value;

  public Integer getValue() {
    return value;
  }
}