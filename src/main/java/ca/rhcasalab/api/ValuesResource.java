package ca.rhcasalab.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.quarkus.security.Authenticated;
import io.quarkus.panache.common.Sort;

@Path("/api/values")
@Produces(MediaType.APPLICATION_JSON)

public class ValuesResource {

    @GET
    @Authenticated
    public List<Moisture> getValues() {

        return Moisture.findAll(Sort.descending("_id")).range(0, 19).list();

    }

    @POST
    public Response setValue(Moisture newValue) {
        newValue.persist();
        return Response.status(201).build();
    }
}